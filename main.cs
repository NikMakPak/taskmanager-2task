using System;
using System.Collections.Generic;
using System.Linq;

public interface ITask
{
    string Name { get; }
    string Description { get; }
    DateTime Deadline { get; }
    bool Completed { get; }
    void Complete();
}

public class Subtask : ITask
{
    public string Name { get; }
    public string Description { get; }
    public DateTime Deadline { get; }
    public bool Completed { get; private set; }

    public Subtask(string name, string description, DateTime deadline)
    {
        Name = name;
        Description = description;
        Deadline = deadline;
        Completed = false;
    }

    public void Complete()
    {
        Completed = true;
    }
}

public class Supertask : ITask
{
    public HashSet<ITask> Subtasks = new();
    public string Name { get; }
    public string Description => string.Empty;
    public DateTime Deadline => Subtasks.Select(task => task.Deadline).OrderBy(deadline => deadline).FirstOrDefault();
    public bool Completed { get; private set; }

    public Supertask(string name)
    {
        Name = name;
        Completed = false;
    }

    public void Complete()
    {
        Completed = true;
        foreach (var subTask in Subtasks)
        {
            subTask.Complete();
            TaskManager.Instance.Done.Subtasks.Add(subTask);
            Subtasks.Remove(subTask);
        }
    }
}

public sealed class TaskManager
{
    private TaskManager() { }
    public static TaskManager Instance { get; } = new();

    public Supertask Todo { get; } = new("Группа задач Todo");
    public Supertask InProgress { get; } = new("Группа задач InProgress");
    public Supertask Done { get; } = new("Группа задач Done");
}

class Program
{
    static void Main(string[] args)
    {
        TaskManager taskManager = TaskManager.Instance;

        Subtask docTask = new Subtask("Подготовить отчетность", "Составить отчетность за прошлый месяц", new DateTime(2023, 06, 09));
        Subtask meetTask = new Subtask("Провести совещание", "Провести совещание с отделами по поводу планов на следующий квартал", new DateTime(2023, 06, 10));
        Subtask pointTask = new Subtask("Подготовить презентацию", "Подготовить презентацию на конференцию", new DateTime(2023, 06, 11));

        Supertask studyTasks = new Supertask("Стартап задачи");

        studyTasks.Subtasks.Add(docTask);
        studyTasks.Subtasks.Add(meetTask);
        studyTasks.Subtasks.Add(pointTask);

        taskManager.Todo.Subtasks.Add(studyTasks);

        Console.WriteLine("Список задач:");
        TaskPrint(taskManager.Todo);

        studyTasks.Complete();
        Console.WriteLine($"\nГруппа задач \"{studyTasks.Name}\" завершена!");

        Console.WriteLine("\nСписок задач после завершения всей группы:");
        TaskPrint(taskManager.Todo);

        Console.WriteLine("\nСписок завершенных задач:");
        TaskPrint(taskManager.Done);

        var manager = TaskManager.Instance;
      TaskManager anotherTaskManager = TaskManager.Instance;
        Console.WriteLine($"taskManager == anotherTaskManager: {manager == anotherTaskManager}");

    }

    public static void TaskPrint(ITask task, int offset = 0)
    {
        if (task is null)
        {
            return;
        }
        else if (task is Supertask)
        {
            Console.WriteLine(new String('-', offset) + $" {task.Deadline} {task.Name} {task.Description}");
            foreach (var subTask in (task as Supertask).Subtasks)
            {
                TaskPrint(subTask, offset + 1);
            }
        }
        else if (task is Subtask)
        {
            Console.WriteLine(new String('-', offset) + $" {task.Deadline} {task.Name} {task.Description}");
        }
    }
}